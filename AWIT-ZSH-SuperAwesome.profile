[Appearance]
AntiAliasFonts=true
BoldIntense=true
ColorScheme=AWIT-ZSH-SuperAwesome
Font=Hack,12,-1,0,50,0,0,0,0,0,Regular
UseFontLineChararacters=false

[Cursor Options]
CursorShape=2

[General]
Command=/bin/zsh
LocalTabTitleFormat=%d  - %n - %w
Name=AWIT ZSH SuperAwesome
Parent=FALLBACK/
RemoteTabTitleFormat=(%u) %H - %w

[Interaction Options]
TrimTrailingSpacesInSelectedText=true

[Scrolling]
HistorySize=10000

[Terminal Features]
BlinkingCursorEnabled=true
