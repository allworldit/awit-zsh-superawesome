# Requirements

  * oh-my-zsh
  * zsh-syntax-highlighting
  * powerlevel9k

# Installing

	# Checkout and initialize repository
	cd ~
	git clone ssh://git@gitlab.devlabs.linuxassist.net:608/allworldit/awit-zsh-superawesome.git .zsh

	# Activate the configuration
	ln -s .zsh/zshrc .zshrc

	# For Konsole
	cp AWIT-ZSH-SuperAwesome.profile ~/.local/share/konsole/
	cp AWIT-ZSH-SuperAwesome.colorscheme ~/.local/share/konsole/

	Select the "AWIT-ZSH-SuperAwesome" profile as default in Konsole's profile manager.

