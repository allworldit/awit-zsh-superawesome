# Set the directory where we're being loaded from
AZS_DIR="$0:h"

## Home bin path

# Override first bin directory
if [ -d "$HOME/.local/bin" ]
then
	if ! [[ "$PATH" =~ "$HOME/.local/bin" ]]
	then
		export PATH="$HOME/.local/bin:$PATH"
	fi
else
	# We should never need this, but just in case
	mkdir -p "$HOME/.local/bin"

	if ! [[ "$PATH" =~ "$HOME/.local/bin" ]]
	then
		export PATH="$HOME/.local/bin:$PATH"
	fi
fi

# Remove duplicates in $PATH, if they exist
typeset -U path

## Configure the history

# The following line changes the command execution time stamp shown in
# the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
HIST_STAMPS='%Y-%m-%d'
# Don't store invocations of 'history' in the history
setopt HIST_NO_STORE
# Enable incremental append
setopt INC_APPEND_HISTORY

## Long running

# If a command takes a combined time of > 10 seconds, zsh will report it...
REPORTTIME=10

## Aliases

# Override grml's setting for dir
alias dir='dir --color=auto'
# Add colour
alias vdir='vdir --color=auto'
alias fgrep='fgrep --color=auto'

# Make sure we ask before every use of our key
alias ssh-add='/usr/bin/ssh-add -c -t 1800'

# Use ksshaskpass if it exists and we're using the plasma desktop
if [[ "$DESKTOP_SESSION" =~ "plasma" ]]
then
	if [ -x /usr/bin/ksshaskpass ]
	then
		# Use KDE askpass
		export SSH_ASKPASS=/usr/bin/ksshaskpass
		echo "[awit-zsh-superawesome] Using ksshaskpass"
	else
		echo "[awit-zsh-superawesome] NOTICE: You're using the plasma desktop, but you don't have ksshaskpass installed?"
	fi
fi

# Use keychain if installed
if [ -x /usr/bin/keychain ]
then
	eval `keychain --confirm --clear --noask --timeout 15 --eval --systemd --stop others --quiet --inherit local-once --agents ssh`
	echo "[awit-zsh-superawesome] Using keychain (15 min timeout, explicit confirmation)"
fi

## Completion

# A handy-dandy function for reloading completions we might hack on
function compreload() {
	ARG="$1"
	unfunction _$ARG
	autoload _$ARG
}

zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _correct _approximate
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' use-compctl false

# Add custom completion scripts
if [ -d "$HOME/.zsh/completion" ]
then
	fpath=($HOME/.zsh/completion $fpath)
fi

## Oh-my-zsh

plugins=(
	colored-man-pages
	colorize
	command-not-found
	gitfast
	screen
	virtualenv
	zsh-syntax-highlighting
)

# Allow theming if we're running in a desktop session
if [[ "$DESKTOP_SESSION" =~ "plasma" && "$TERM_PROGRAM" != "vscode" ]]
then
	# Set oh my zsh theme
	ZSH_THEME="powerlevel10k"

	# Setup powerlevel9k
	POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(context dir dir_writable vcs virtualenv)
	POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status root_indicator command_execution_time background_jobs history time)
	POWERLEVEL9K_MODE=awesome-fontconfig

	# Shorten directory length
	POWERLEVEL9K_SHORTEN_DIR_LENGTH=2

	# Fix up icons a bit
	POWERLEVEL9K_OS_ICON_BACKGROUND="white"
	POWERLEVEL9K_OS_ICON_FOREGROUND="blue"
	POWERLEVEL9K_DIR_HOME_FOREGROUND="white"
	POWERLEVEL9K_DIR_HOME_SUBFOLDER_FOREGROUND="white"
	POWERLEVEL9K_DIR_DEFAULT_FOREGROUND="white"

	# Add highlight color to prompt
	ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets pattern cursor)

	typeset -gA ZSH_HIGHLIGHT_STYLES
	ZSH_HIGHLIGHT_STYLES[cursor]='bold'
	ZSH_HIGHLIGHT_STYLES[alias]='fg=green,bold'
	ZSH_HIGHLIGHT_STYLES[suffix-alias]='fg=green,bold'
	ZSH_HIGHLIGHT_STYLES[builtin]='fg=green,bold'
	ZSH_HIGHLIGHT_STYLES[function]='fg=green,bold'
	ZSH_HIGHLIGHT_STYLES[command]='fg=green,bold'
	ZSH_HIGHLIGHT_STYLES[precommand]='fg=green,bold'
	ZSH_HIGHLIGHT_STYLES[hashed-command]='fg=green,bold'
fi

## Variables

# Useful variables for our ZSH environment
test "x$HOSTNAME" = "x" && export HOSTNAME="$HOST"

export GIT_EDITOR="$EDITOR"
export VISUAL="$EDITOR"

## Load Oh-my-zsh [This needs to be last]

# Path to your oh-my-zsh installation
ZSH=/usr/share/oh-my-zsh/

# Custom directory
ZSH_CUSTOM="$AZS_DIR/custom"

# Disable bi-weekly auto-update checks
DISABLE_AUTO_UPDATE="true"

COMPLETION_WAITING_DOTS="true"

# Stop the annoying habit of creating multiple dump files
ZSH_COMPDUMP="$COMPDUMPFILE"

# Pull in the system oh-my-zsh
source "$ZSH/oh-my-zsh.sh"
